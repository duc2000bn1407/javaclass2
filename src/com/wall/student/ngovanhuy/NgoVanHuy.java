package com.wall.baitap.student.ngovanhuy;

import java.util.Scanner;

public class NgoVanHuy {
    public static boolean checkSCP(int n) {
        int kt = (int) Math.sqrt(n);
        if (kt * kt == n) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int n;
        System.out.print("Nhập n: ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        boolean soCP = checkSCP(n);
        if (soCP) {
            System.out.println(n + "Là số chính phương");
        } else {
            System.out.println(n + "Không là số chính phương");
        }
    }
}
