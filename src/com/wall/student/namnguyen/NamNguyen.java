package com.wall.baitap.student.namnguyen;

import java.util.Scanner;

public class NamNguyen {
    public static void main(String[] args) {
        int n;
        while (true) {
            System.out.print("Nhap n= ");
            Scanner sc = new Scanner(System.in);
            n = sc.nextInt();
            if (n <= 0) break;
        }

        boolean res = checkSCP(n);
        if (res) {
            System.out.println(n + "\nLa so chinh phuong");
        } else {
            System.out.println(n + "\nKhong la so chinh phuong");
        }
    }

    public static boolean checkSCP(int n) {
        int sqr = (int) Math.sqrt(n);
        if (sqr * sqr == n) {
            return true;
        }
        return false;
    }
}

