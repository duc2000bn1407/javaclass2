package com.wall.baitap.student.buiquangtrung;

import java.util.Scanner;

public class BuiQuangTrung {
    static boolean kiemTraSoChinhPhuong(double a) {
        double sq = Math.sqrt(a);
        return sq - (int) sq == 0;
    }

    public static void main(String[] args) {
        System.out.print("Nhap so :");
        Scanner scanner = new Scanner(System.in);  // số lượng mở file nhất định
        double num = scanner.nextDouble();
        scanner.close();
        if (kiemTraSoChinhPhuong(num)) {  // Ctrl + Alt + L format code   => ctrl + shift + mũi tên trái
            System.out.println(num + "La so chinh phuong");
        } else {
            System.out.println(num + "Khong phai la so chinh phuong");
        }
    }
}
