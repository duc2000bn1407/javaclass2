package com.wall.java1.baitap1;

import java.util.Scanner;

public class BaiSo5Fibanaci {
    //data
    public static void main(String[] args) {
        // bien trong block trong stack  ->  giai phong
        System.out.println("Nhập số lượng số fibonaci cần xử lý: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int d = process(n);
        System.out.println("Giá trị của số fibobnaci thu n là: " + d);
    }

    // chậm  -> không xử được trường hợp n lớn > 1000000
    static int process(int m) {
        // xu ly trong day
        int[] f = new int[m + 1];
        f[0] = 1;
        f[1] = 1;
        // đọc code từng dòng để duỗi vòng for ra để hiểu vấn đề => chạy từng lệnh 1
        int i = 1;
        while (i++ <= m) {
            f[i] = f[i - 1] + f[i - 2];
        }
        return f[m];
    }

}
